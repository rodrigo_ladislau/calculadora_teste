﻿namespace Calculadora
{
    partial class Padrao
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Padrao));
            this.txtValor = new System.Windows.Forms.TextBox();
            this.btnUm = new System.Windows.Forms.Button();
            this.btnDois = new System.Windows.Forms.Button();
            this.btnTres = new System.Windows.Forms.Button();
            this.btnSoma = new System.Windows.Forms.Button();
            this.btnSub = new System.Windows.Forms.Button();
            this.btnSeis = new System.Windows.Forms.Button();
            this.btnCinco = new System.Windows.Forms.Button();
            this.btnQuatro = new System.Windows.Forms.Button();
            this.btnMult = new System.Windows.Forms.Button();
            this.btnNove = new System.Windows.Forms.Button();
            this.btnOito = new System.Windows.Forms.Button();
            this.btnSete = new System.Windows.Forms.Button();
            this.btnDiv = new System.Windows.Forms.Button();
            this.btnIgual = new System.Windows.Forms.Button();
            this.btnZero = new System.Windows.Forms.Button();
            this.btnLimpar = new System.Windows.Forms.Button();
            this.BtnBackSpace = new System.Windows.Forms.Button();
            this.BtnPonto = new System.Windows.Forms.Button();
            this.BtnPi = new System.Windows.Forms.Button();
            this.BtnInt = new System.Windows.Forms.Button();
            this.BtnMod = new System.Windows.Forms.Button();
            this.BtnLog = new System.Windows.Forms.Button();
            this.BtnDezElevado = new System.Windows.Forms.Button();
            this.BtnAoCubo = new System.Windows.Forms.Button();
            this.BtnAoQuadrado = new System.Windows.Forms.Button();
            this.BtnPotencia = new System.Windows.Forms.Button();
            this.BtnFatorial = new System.Windows.Forms.Button();
            this.BtnSin = new System.Windows.Forms.Button();
            this.BtnCos = new System.Windows.Forms.Button();
            this.BtnTan = new System.Windows.Forms.Button();
            this.BtnExp = new System.Windows.Forms.Button();
            this.BtnSinh = new System.Windows.Forms.Button();
            this.BtnCosh = new System.Windows.Forms.Button();
            this.BtnTanh = new System.Windows.Forms.Button();
            this.BtnPorcentagem = new System.Windows.Forms.Button();
            this.BtnTrocarSinal = new System.Windows.Forms.Button();
            this.BtnRaizQuadrada = new System.Windows.Forms.Button();
            this.BtnUmDividoPor = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // txtValor
            // 
            this.txtValor.Cursor = System.Windows.Forms.Cursors.Default;
            this.txtValor.Enabled = false;
            this.txtValor.Location = new System.Drawing.Point(12, 12);
            this.txtValor.Name = "txtValor";
            this.txtValor.Size = new System.Drawing.Size(194, 20);
            this.txtValor.TabIndex = 0;
            // 
            // btnUm
            // 
            this.btnUm.Location = new System.Drawing.Point(12, 125);
            this.btnUm.Name = "btnUm";
            this.btnUm.Size = new System.Drawing.Size(44, 23);
            this.btnUm.TabIndex = 1;
            this.btnUm.Text = "1";
            this.btnUm.UseVisualStyleBackColor = true;
            this.btnUm.Click += new System.EventHandler(this.adicionarValor);
            // 
            // btnDois
            // 
            this.btnDois.Location = new System.Drawing.Point(62, 125);
            this.btnDois.Name = "btnDois";
            this.btnDois.Size = new System.Drawing.Size(44, 23);
            this.btnDois.TabIndex = 2;
            this.btnDois.Text = "2";
            this.btnDois.UseVisualStyleBackColor = true;
            this.btnDois.Click += new System.EventHandler(this.adicionarValor);
            // 
            // btnTres
            // 
            this.btnTres.Location = new System.Drawing.Point(112, 125);
            this.btnTres.Name = "btnTres";
            this.btnTres.Size = new System.Drawing.Size(44, 23);
            this.btnTres.TabIndex = 3;
            this.btnTres.Text = "3";
            this.btnTres.UseVisualStyleBackColor = true;
            this.btnTres.Click += new System.EventHandler(this.adicionarValor);
            // 
            // btnSoma
            // 
            this.btnSoma.Location = new System.Drawing.Point(162, 67);
            this.btnSoma.Name = "btnSoma";
            this.btnSoma.Size = new System.Drawing.Size(44, 23);
            this.btnSoma.TabIndex = 4;
            this.btnSoma.Text = "+";
            this.btnSoma.UseVisualStyleBackColor = true;
            this.btnSoma.Click += new System.EventHandler(this.setarOperador);
            // 
            // btnSub
            // 
            this.btnSub.Location = new System.Drawing.Point(162, 96);
            this.btnSub.Name = "btnSub";
            this.btnSub.Size = new System.Drawing.Size(44, 23);
            this.btnSub.TabIndex = 9;
            this.btnSub.Text = "-";
            this.btnSub.UseVisualStyleBackColor = true;
            this.btnSub.Click += new System.EventHandler(this.setarOperador);
            // 
            // btnSeis
            // 
            this.btnSeis.Location = new System.Drawing.Point(112, 96);
            this.btnSeis.Name = "btnSeis";
            this.btnSeis.Size = new System.Drawing.Size(44, 23);
            this.btnSeis.TabIndex = 8;
            this.btnSeis.Text = "6";
            this.btnSeis.UseVisualStyleBackColor = true;
            this.btnSeis.Click += new System.EventHandler(this.adicionarValor);
            // 
            // btnCinco
            // 
            this.btnCinco.Location = new System.Drawing.Point(62, 96);
            this.btnCinco.Name = "btnCinco";
            this.btnCinco.Size = new System.Drawing.Size(44, 23);
            this.btnCinco.TabIndex = 7;
            this.btnCinco.Text = "5";
            this.btnCinco.UseVisualStyleBackColor = true;
            this.btnCinco.Click += new System.EventHandler(this.adicionarValor);
            // 
            // btnQuatro
            // 
            this.btnQuatro.Location = new System.Drawing.Point(12, 96);
            this.btnQuatro.Name = "btnQuatro";
            this.btnQuatro.Size = new System.Drawing.Size(44, 23);
            this.btnQuatro.TabIndex = 6;
            this.btnQuatro.Text = "4";
            this.btnQuatro.UseVisualStyleBackColor = true;
            this.btnQuatro.Click += new System.EventHandler(this.adicionarValor);
            // 
            // btnMult
            // 
            this.btnMult.Location = new System.Drawing.Point(162, 125);
            this.btnMult.Name = "btnMult";
            this.btnMult.Size = new System.Drawing.Size(44, 23);
            this.btnMult.TabIndex = 14;
            this.btnMult.Text = "*";
            this.btnMult.UseVisualStyleBackColor = true;
            this.btnMult.Click += new System.EventHandler(this.setarOperador);
            // 
            // btnNove
            // 
            this.btnNove.Location = new System.Drawing.Point(112, 67);
            this.btnNove.Name = "btnNove";
            this.btnNove.Size = new System.Drawing.Size(44, 23);
            this.btnNove.TabIndex = 13;
            this.btnNove.Text = "9";
            this.btnNove.UseVisualStyleBackColor = true;
            this.btnNove.Click += new System.EventHandler(this.adicionarValor);
            // 
            // btnOito
            // 
            this.btnOito.Location = new System.Drawing.Point(62, 67);
            this.btnOito.Name = "btnOito";
            this.btnOito.Size = new System.Drawing.Size(44, 23);
            this.btnOito.TabIndex = 12;
            this.btnOito.Text = "8";
            this.btnOito.UseVisualStyleBackColor = true;
            this.btnOito.Click += new System.EventHandler(this.adicionarValor);
            // 
            // btnSete
            // 
            this.btnSete.Location = new System.Drawing.Point(12, 67);
            this.btnSete.Name = "btnSete";
            this.btnSete.Size = new System.Drawing.Size(44, 23);
            this.btnSete.TabIndex = 11;
            this.btnSete.Text = "7";
            this.btnSete.UseVisualStyleBackColor = true;
            this.btnSete.Click += new System.EventHandler(this.adicionarValor);
            // 
            // btnDiv
            // 
            this.btnDiv.Location = new System.Drawing.Point(162, 154);
            this.btnDiv.Name = "btnDiv";
            this.btnDiv.Size = new System.Drawing.Size(44, 23);
            this.btnDiv.TabIndex = 19;
            this.btnDiv.Text = "/";
            this.btnDiv.UseVisualStyleBackColor = true;
            this.btnDiv.Click += new System.EventHandler(this.setarOperador);
            // 
            // btnIgual
            // 
            this.btnIgual.Location = new System.Drawing.Point(12, 183);
            this.btnIgual.Name = "btnIgual";
            this.btnIgual.Size = new System.Drawing.Size(194, 23);
            this.btnIgual.TabIndex = 18;
            this.btnIgual.Text = "=";
            this.btnIgual.UseVisualStyleBackColor = true;
            this.btnIgual.Click += new System.EventHandler(this.btnIgual_Click);
            // 
            // btnZero
            // 
            this.btnZero.Location = new System.Drawing.Point(62, 154);
            this.btnZero.Name = "btnZero";
            this.btnZero.Size = new System.Drawing.Size(44, 23);
            this.btnZero.TabIndex = 17;
            this.btnZero.Text = "0";
            this.btnZero.UseVisualStyleBackColor = true;
            this.btnZero.Click += new System.EventHandler(this.adicionarValor);
            // 
            // btnLimpar
            // 
            this.btnLimpar.Location = new System.Drawing.Point(12, 154);
            this.btnLimpar.Name = "btnLimpar";
            this.btnLimpar.Size = new System.Drawing.Size(44, 23);
            this.btnLimpar.TabIndex = 16;
            this.btnLimpar.Text = "C";
            this.btnLimpar.UseVisualStyleBackColor = true;
            this.btnLimpar.Click += new System.EventHandler(this.btnLimpar_Click);
            // 
            // BtnBackSpace
            // 
            this.BtnBackSpace.Location = new System.Drawing.Point(12, 38);
            this.BtnBackSpace.Name = "BtnBackSpace";
            this.BtnBackSpace.Size = new System.Drawing.Size(194, 23);
            this.BtnBackSpace.TabIndex = 20;
            this.BtnBackSpace.Text = "BackSpace";
            this.BtnBackSpace.UseVisualStyleBackColor = true;
            this.BtnBackSpace.Click += new System.EventHandler(this.BtnBackSpace_Click);
            // 
            // BtnPonto
            // 
            this.BtnPonto.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BtnPonto.Location = new System.Drawing.Point(112, 154);
            this.BtnPonto.Name = "BtnPonto";
            this.BtnPonto.Size = new System.Drawing.Size(44, 23);
            this.BtnPonto.TabIndex = 21;
            this.BtnPonto.Text = ",";
            this.BtnPonto.UseVisualStyleBackColor = true;
            this.BtnPonto.Click += new System.EventHandler(this.adicionarValor);
            // 
            // BtnPi
            // 
            this.BtnPi.Location = new System.Drawing.Point(12, 212);
            this.BtnPi.Name = "BtnPi";
            this.BtnPi.Size = new System.Drawing.Size(44, 23);
            this.BtnPi.TabIndex = 22;
            this.BtnPi.Text = "PI";
            this.BtnPi.UseVisualStyleBackColor = true;
            this.BtnPi.Click += new System.EventHandler(this.BtnPi_Click);
            // 
            // BtnInt
            // 
            this.BtnInt.Location = new System.Drawing.Point(63, 211);
            this.BtnInt.Name = "BtnInt";
            this.BtnInt.Size = new System.Drawing.Size(43, 23);
            this.BtnInt.TabIndex = 23;
            this.BtnInt.Text = "Int";
            this.BtnInt.UseVisualStyleBackColor = true;
            this.BtnInt.Click += new System.EventHandler(this.BtnInt_Click);
            // 
            // BtnMod
            // 
            this.BtnMod.Location = new System.Drawing.Point(112, 212);
            this.BtnMod.Name = "BtnMod";
            this.BtnMod.Size = new System.Drawing.Size(44, 23);
            this.BtnMod.TabIndex = 24;
            this.BtnMod.Text = "Mod";
            this.BtnMod.UseVisualStyleBackColor = true;
            this.BtnMod.Click += new System.EventHandler(this.setarOperador);
            // 
            // BtnLog
            // 
            this.BtnLog.Location = new System.Drawing.Point(162, 212);
            this.BtnLog.Name = "BtnLog";
            this.BtnLog.Size = new System.Drawing.Size(44, 23);
            this.BtnLog.TabIndex = 25;
            this.BtnLog.Text = "Log";
            this.BtnLog.UseVisualStyleBackColor = true;
            this.BtnLog.Click += new System.EventHandler(this.BtnLog_Click);
            // 
            // BtnDezElevado
            // 
            this.BtnDezElevado.Location = new System.Drawing.Point(162, 241);
            this.BtnDezElevado.Name = "BtnDezElevado";
            this.BtnDezElevado.Size = new System.Drawing.Size(44, 23);
            this.BtnDezElevado.TabIndex = 29;
            this.BtnDezElevado.Text = "10^X";
            this.BtnDezElevado.UseVisualStyleBackColor = true;
            this.BtnDezElevado.Click += new System.EventHandler(this.BtnDezElevado_Click);
            // 
            // BtnAoCubo
            // 
            this.BtnAoCubo.Location = new System.Drawing.Point(112, 241);
            this.BtnAoCubo.Name = "BtnAoCubo";
            this.BtnAoCubo.Size = new System.Drawing.Size(44, 23);
            this.BtnAoCubo.TabIndex = 28;
            this.BtnAoCubo.Text = "X³";
            this.BtnAoCubo.UseVisualStyleBackColor = true;
            this.BtnAoCubo.Click += new System.EventHandler(this.BtnAoCubo_Click);
            // 
            // BtnAoQuadrado
            // 
            this.BtnAoQuadrado.Location = new System.Drawing.Point(63, 240);
            this.BtnAoQuadrado.Name = "BtnAoQuadrado";
            this.BtnAoQuadrado.Size = new System.Drawing.Size(43, 23);
            this.BtnAoQuadrado.TabIndex = 27;
            this.BtnAoQuadrado.Text = "X²";
            this.BtnAoQuadrado.UseVisualStyleBackColor = true;
            this.BtnAoQuadrado.Click += new System.EventHandler(this.BtnAoQuadrado_Click);
            // 
            // BtnPotencia
            // 
            this.BtnPotencia.Location = new System.Drawing.Point(12, 241);
            this.BtnPotencia.Name = "BtnPotencia";
            this.BtnPotencia.Size = new System.Drawing.Size(44, 23);
            this.BtnPotencia.TabIndex = 26;
            this.BtnPotencia.Text = "X^Y";
            this.BtnPotencia.UseVisualStyleBackColor = true;
            this.BtnPotencia.Click += new System.EventHandler(this.BtnPotencia_Click);
            // 
            // BtnFatorial
            // 
            this.BtnFatorial.Location = new System.Drawing.Point(12, 271);
            this.BtnFatorial.Name = "BtnFatorial";
            this.BtnFatorial.Size = new System.Drawing.Size(44, 23);
            this.BtnFatorial.TabIndex = 30;
            this.BtnFatorial.Text = "n!";
            this.BtnFatorial.UseVisualStyleBackColor = true;
            this.BtnFatorial.Click += new System.EventHandler(this.BtnFatorial_Click);
            // 
            // BtnSin
            // 
            this.BtnSin.Location = new System.Drawing.Point(62, 271);
            this.BtnSin.Name = "BtnSin";
            this.BtnSin.Size = new System.Drawing.Size(44, 23);
            this.BtnSin.TabIndex = 31;
            this.BtnSin.Text = "sin";
            this.BtnSin.UseVisualStyleBackColor = true;
            this.BtnSin.Click += new System.EventHandler(this.BtnSin_Click);
            // 
            // BtnCos
            // 
            this.BtnCos.Location = new System.Drawing.Point(112, 270);
            this.BtnCos.Name = "BtnCos";
            this.BtnCos.Size = new System.Drawing.Size(44, 23);
            this.BtnCos.TabIndex = 32;
            this.BtnCos.Text = "cos";
            this.BtnCos.UseVisualStyleBackColor = true;
            this.BtnCos.Click += new System.EventHandler(this.BtnCos_Click);
            // 
            // BtnTan
            // 
            this.BtnTan.Location = new System.Drawing.Point(162, 270);
            this.BtnTan.Name = "BtnTan";
            this.BtnTan.Size = new System.Drawing.Size(44, 23);
            this.BtnTan.TabIndex = 33;
            this.BtnTan.Text = "tan";
            this.BtnTan.UseVisualStyleBackColor = true;
            this.BtnTan.Click += new System.EventHandler(this.BtnTan_Click);
            // 
            // BtnExp
            // 
            this.BtnExp.Location = new System.Drawing.Point(12, 300);
            this.BtnExp.Name = "BtnExp";
            this.BtnExp.Size = new System.Drawing.Size(44, 23);
            this.BtnExp.TabIndex = 34;
            this.BtnExp.Text = "Exp";
            this.BtnExp.UseVisualStyleBackColor = true;
            this.BtnExp.Click += new System.EventHandler(this.BtnExp_Click);
            // 
            // BtnSinh
            // 
            this.BtnSinh.Location = new System.Drawing.Point(62, 300);
            this.BtnSinh.Name = "BtnSinh";
            this.BtnSinh.Size = new System.Drawing.Size(44, 23);
            this.BtnSinh.TabIndex = 35;
            this.BtnSinh.Text = "sinh";
            this.BtnSinh.UseVisualStyleBackColor = true;
            this.BtnSinh.Click += new System.EventHandler(this.BtnSinh_Click);
            // 
            // BtnCosh
            // 
            this.BtnCosh.Location = new System.Drawing.Point(112, 300);
            this.BtnCosh.Name = "BtnCosh";
            this.BtnCosh.Size = new System.Drawing.Size(44, 23);
            this.BtnCosh.TabIndex = 36;
            this.BtnCosh.Text = "cosh";
            this.BtnCosh.UseVisualStyleBackColor = true;
            this.BtnCosh.Click += new System.EventHandler(this.BtnCosh_Click);
            // 
            // BtnTanh
            // 
            this.BtnTanh.Location = new System.Drawing.Point(162, 300);
            this.BtnTanh.Name = "BtnTanh";
            this.BtnTanh.Size = new System.Drawing.Size(44, 23);
            this.BtnTanh.TabIndex = 37;
            this.BtnTanh.Text = "tanh";
            this.BtnTanh.UseVisualStyleBackColor = true;
            this.BtnTanh.Click += new System.EventHandler(this.BtnTanh_Click);
            // 
            // BtnPorcentagem
            // 
            this.BtnPorcentagem.Location = new System.Drawing.Point(62, 328);
            this.BtnPorcentagem.Name = "BtnPorcentagem";
            this.BtnPorcentagem.Size = new System.Drawing.Size(44, 23);
            this.BtnPorcentagem.TabIndex = 38;
            this.BtnPorcentagem.Text = "%";
            this.BtnPorcentagem.UseVisualStyleBackColor = true;
            this.BtnPorcentagem.Click += new System.EventHandler(this.BtnPorcentagem_Click);
            // 
            // BtnTrocarSinal
            // 
            this.BtnTrocarSinal.Location = new System.Drawing.Point(12, 328);
            this.BtnTrocarSinal.Name = "BtnTrocarSinal";
            this.BtnTrocarSinal.Size = new System.Drawing.Size(44, 23);
            this.BtnTrocarSinal.TabIndex = 39;
            this.BtnTrocarSinal.Text = "+ -";
            this.BtnTrocarSinal.UseVisualStyleBackColor = true;
            this.BtnTrocarSinal.Click += new System.EventHandler(this.BtnTrocarSinal_Click);
            // 
            // BtnRaizQuadrada
            // 
            this.BtnRaizQuadrada.Location = new System.Drawing.Point(112, 328);
            this.BtnRaizQuadrada.Name = "BtnRaizQuadrada";
            this.BtnRaizQuadrada.Size = new System.Drawing.Size(44, 23);
            this.BtnRaizQuadrada.TabIndex = 40;
            this.BtnRaizQuadrada.Text = "Raiz";
            this.BtnRaizQuadrada.UseVisualStyleBackColor = true;
            this.BtnRaizQuadrada.Click += new System.EventHandler(this.BtnRaizQuadrada_Click);
            // 
            // BtnUmDividoPor
            // 
            this.BtnUmDividoPor.Location = new System.Drawing.Point(162, 328);
            this.BtnUmDividoPor.Name = "BtnUmDividoPor";
            this.BtnUmDividoPor.Size = new System.Drawing.Size(44, 23);
            this.BtnUmDividoPor.TabIndex = 41;
            this.BtnUmDividoPor.Text = "1/x";
            this.BtnUmDividoPor.UseVisualStyleBackColor = true;
            this.BtnUmDividoPor.Click += new System.EventHandler(this.BtnUmDividoPor_Click);
            // 
            // Padrao
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(218, 363);
            this.Controls.Add(this.BtnUmDividoPor);
            this.Controls.Add(this.BtnRaizQuadrada);
            this.Controls.Add(this.BtnTrocarSinal);
            this.Controls.Add(this.BtnPorcentagem);
            this.Controls.Add(this.BtnTanh);
            this.Controls.Add(this.BtnCosh);
            this.Controls.Add(this.BtnSinh);
            this.Controls.Add(this.BtnExp);
            this.Controls.Add(this.BtnTan);
            this.Controls.Add(this.BtnCos);
            this.Controls.Add(this.BtnSin);
            this.Controls.Add(this.BtnFatorial);
            this.Controls.Add(this.BtnDezElevado);
            this.Controls.Add(this.BtnAoCubo);
            this.Controls.Add(this.BtnAoQuadrado);
            this.Controls.Add(this.BtnPotencia);
            this.Controls.Add(this.BtnLog);
            this.Controls.Add(this.BtnMod);
            this.Controls.Add(this.BtnInt);
            this.Controls.Add(this.BtnPi);
            this.Controls.Add(this.BtnPonto);
            this.Controls.Add(this.BtnBackSpace);
            this.Controls.Add(this.btnDiv);
            this.Controls.Add(this.btnIgual);
            this.Controls.Add(this.btnZero);
            this.Controls.Add(this.btnLimpar);
            this.Controls.Add(this.btnMult);
            this.Controls.Add(this.btnNove);
            this.Controls.Add(this.btnOito);
            this.Controls.Add(this.btnSete);
            this.Controls.Add(this.btnSub);
            this.Controls.Add(this.btnSeis);
            this.Controls.Add(this.btnCinco);
            this.Controls.Add(this.btnQuatro);
            this.Controls.Add(this.btnSoma);
            this.Controls.Add(this.btnTres);
            this.Controls.Add(this.btnDois);
            this.Controls.Add(this.btnUm);
            this.Controls.Add(this.txtValor);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "Padrao";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Calculadora";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.TextBox txtValor;
        private System.Windows.Forms.Button btnUm;
        private System.Windows.Forms.Button btnDois;
        private System.Windows.Forms.Button btnTres;
        private System.Windows.Forms.Button btnSoma;
        private System.Windows.Forms.Button btnSub;
        private System.Windows.Forms.Button btnSeis;
        private System.Windows.Forms.Button btnCinco;
        private System.Windows.Forms.Button btnQuatro;
        private System.Windows.Forms.Button btnMult;
        private System.Windows.Forms.Button btnNove;
        private System.Windows.Forms.Button btnOito;
        private System.Windows.Forms.Button btnSete;
        private System.Windows.Forms.Button btnDiv;
        private System.Windows.Forms.Button btnIgual;
        private System.Windows.Forms.Button btnZero;
        private System.Windows.Forms.Button btnLimpar;
        private System.Windows.Forms.Button BtnBackSpace;
        private System.Windows.Forms.Button BtnPonto;
        private System.Windows.Forms.Button BtnPi;
        private System.Windows.Forms.Button BtnInt;
        private System.Windows.Forms.Button BtnMod;
        private System.Windows.Forms.Button BtnLog;
        private System.Windows.Forms.Button BtnDezElevado;
        private System.Windows.Forms.Button BtnAoCubo;
        private System.Windows.Forms.Button BtnAoQuadrado;
        private System.Windows.Forms.Button BtnPotencia;
        private System.Windows.Forms.Button BtnFatorial;
        private System.Windows.Forms.Button BtnSin;
        private System.Windows.Forms.Button BtnCos;
        private System.Windows.Forms.Button BtnTan;
        private System.Windows.Forms.Button BtnExp;
        private System.Windows.Forms.Button BtnSinh;
        private System.Windows.Forms.Button BtnCosh;
        private System.Windows.Forms.Button BtnTanh;
        private System.Windows.Forms.Button BtnPorcentagem;
        private System.Windows.Forms.Button BtnTrocarSinal;
        private System.Windows.Forms.Button BtnRaizQuadrada;
        private System.Windows.Forms.Button BtnUmDividoPor;
    }
}

