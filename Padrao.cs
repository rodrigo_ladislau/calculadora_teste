﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Calculadora
{
    public partial class Padrao : Form
    {
        string operador = "+";
        bool acaoClicada = true;
        double numeroDigitado = 0;
        double total = 0;

        public Padrao()
        {
            InitializeComponent();
            txtValor.Text = "0";
        }

        public void Calcular()
        {
            switch (operador)
            {
                case "+":
                    total += numeroDigitado;
                    break;
                case "-":
                    total -= numeroDigitado;
                    break;
                case "/":
                    if (numeroDigitado != 0)
                        total /= numeroDigitado;
                    else
                        ExibirMensagemErro("Não é possível divisão por zero!");
                    break;
                case "*":
                    total *= numeroDigitado;
                    break;
                case "Mod":
                    total = (total % numeroDigitado);
                    break;
                case "POT":
                    total = Math.Pow(total, numeroDigitado);
                    break;
                case "POT2":
                    total = Math.Pow(numeroDigitado, 2);
                    break;
                case "POT3":
                    total = Math.Pow(numeroDigitado, 3);
                    break;
                case "POT10":
                    total = Math.Pow(10, numeroDigitado);
                    break;
                case "FAT":
                    total = CalcularFatorial(numeroDigitado);
                    break;
                case "SIN":
                    total = Math.Sin(numeroDigitado);
                    break;
                case "SINH":
                    total = Math.Sinh(numeroDigitado);
                    break;
                case "COS":
                    total = Math.Cos(numeroDigitado);
                    break;
                case "COSH":
                    total = Math.Cosh(numeroDigitado);
                    break;
                case "TAN":
                    total = Math.Tan(numeroDigitado);
                    break;
                case "TANH":
                    total = Math.Tanh(numeroDigitado);
                    break;
                case "EXP":
                    total = Math.Exp(numeroDigitado);
                    break;
                default:
                    total = 0;
                    break;
            }

            txtValor.Text = total.ToString();
        }

        public void ExibirMensagemErro(string msg)
        {
            MessageBox.Show(msg);
            operador = "";
            acaoClicada = true;
        }

        public double CalcularFatorial(double numero)
        {
            int convt = 0;
            if (int.TryParse(numero.ToString(), out convt))
            {
                int resp = 1;
                for (int i = 1; i <= convt; i++)
                {
                    resp *= i;
                }
                return Convert.ToDouble(resp);
            }
            else
            {
                ExibirMensagemErro("Erro na conversão!");
                return 0;
            }

        }

        public void ProcessamentoPadrao()
        {
            PegarNumeroDigitado();
            Calcular();
            operador = "+";
            total = 0;
        }

        public void Limpar()
        {
            txtValor.Text = "0";
            operador = "+";
            total = 0;
            numeroDigitado = 0;
            acaoClicada = true;
        }

        public void PegarNumeroDigitado()
        {
            if (Verificar())
            {
                if (!double.TryParse(txtValor.Text, out numeroDigitado))
                {
                    ExibirMensagemErro("Erro na conversão!");
                }
            }
        }

        public Boolean Verificar()
        {
            if (String.IsNullOrWhiteSpace(txtValor.Text))
            {
                ExibirMensagemErro("Valor é Nulo ou contém Espaços em branco!");
                return false;
            }

            return true;
        }

        private void adicionarValor(object sender, EventArgs e)
        {
            string digito = (sender as Button).Text;

            if (!acaoClicada)
                txtValor.Text += digito;
            else
                txtValor.Text = digito;

            acaoClicada = false;
        }

        private void setarOperador(object sender, EventArgs e)
        {
            acaoClicada = true;
            PegarNumeroDigitado();
            Calcular();
            operador = (sender as Button).Text;
        }

        private void btnIgual_Click(object sender, EventArgs e)
        {
            ProcessamentoPadrao();
        }

        private void btnLimpar_Click(object sender, EventArgs e)
        {
            Limpar();
        }

        private void BtnBackSpace_Click(object sender, EventArgs e)
        {
            string txt = txtValor.Text;
            if (!String.IsNullOrWhiteSpace(txt))
            {
                int qtdCaracteres = txt.Length;
                if (qtdCaracteres > 1)
                    txtValor.Text = txt.Substring(0, (qtdCaracteres - 1));
                else
                    txtValor.Text = "0";
            }
        }

        private void BtnPi_Click(object sender, EventArgs e)
        {
            txtValor.Text = Math.PI.ToString();
            acaoClicada = true;
        }

        private void BtnInt_Click(object sender, EventArgs e)
        {
            if (Verificar())
            {
                string txt = txtValor.Text;
                double num = Convert.ToDouble(txt);
                int inteiro = (int)num;
                txtValor.Text = inteiro.ToString();
            }
        }

        private void BtnLog_Click(object sender, EventArgs e)
        {
            if (Verificar())
            {
                double numero = 0;
                if (double.TryParse(txtValor.Text, out numero))
                {
                    double log = Math.Log10(numero);
                    txtValor.Text = log.ToString();
                }
            }
        }

        private void BtnPotencia_Click(object sender, EventArgs e)
        {
            PegarNumeroDigitado();
            Calcular();
            operador = "POT";
            acaoClicada = true;
        }

        private void BtnAoQuadrado_Click(object sender, EventArgs e)
        {
            operador = "POT2";
            ProcessamentoPadrao();
        }

        private void BtnAoCubo_Click(object sender, EventArgs e)
        {
            operador = "POT3";
            ProcessamentoPadrao();
        }

        private void BtnDezElevado_Click(object sender, EventArgs e)
        {
            operador = "POT10";
            ProcessamentoPadrao();
        }

        private void BtnFatorial_Click(object sender, EventArgs e)
        {
            operador = "FAT";
            ProcessamentoPadrao();
        }

        private void BtnSin_Click(object sender, EventArgs e)
        {
            operador = "SIN";
            ProcessamentoPadrao();
        }

        private void BtnCos_Click(object sender, EventArgs e)
        {
            operador = "COS";
            ProcessamentoPadrao();
        }

        private void BtnTan_Click(object sender, EventArgs e)
        {
            operador = "TAN";
            ProcessamentoPadrao();
        }

        private void BtnExp_Click(object sender, EventArgs e)
        {
            operador = "EXP";
            ProcessamentoPadrao();
        }

        private void BtnSinh_Click(object sender, EventArgs e)
        {
            operador = "SINH";
            ProcessamentoPadrao();
        }

        private void BtnCosh_Click(object sender, EventArgs e)
        {
            operador = "COSH";
            ProcessamentoPadrao();
        }

        private void BtnTanh_Click(object sender, EventArgs e)
        {
            operador = "TANH";
            ProcessamentoPadrao();
        }

        private void BtnPorcentagem_Click(object sender, EventArgs e)
        {
            PegarNumeroDigitado();
            double porctg = (total * numeroDigitado) / 100;
            txtValor.Text = porctg.ToString();
        }

        private void BtnTrocarSinal_Click(object sender, EventArgs e)
        {
            PegarNumeroDigitado();
            double numero = numeroDigitado * (-1);
            txtValor.Text = numero.ToString();
        }

        private void BtnRaizQuadrada_Click(object sender, EventArgs e)
        {
            PegarNumeroDigitado();
            double numero = Math.Sqrt(numeroDigitado);
            txtValor.Text = numero.ToString();
        }

        private void BtnUmDividoPor_Click(object sender, EventArgs e)
        {
            PegarNumeroDigitado();
            if (numeroDigitado != 0)
            {
                double numero = 1 / numeroDigitado;
                txtValor.Text = numero.ToString();
            }
            else
            {
                ExibirMensagemErro("Impossível dividir por zero!");
            }
        }
    }
}
